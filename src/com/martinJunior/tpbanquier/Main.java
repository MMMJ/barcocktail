package com.martinJunior.tpbanquier;
import com.martinJunior.tpbanquier.metier.compte.Compt;
import com.sun.imageio.plugins.wbmp.WBMPImageReader;

import java.io.PrintStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Compt compteDePaul = new Compt(3000, "PAUL");
        Compt compteDeJean = new Compt(4000, "JEAN");

        /* depot de 500EURO sur compte de Jean*/
        compteDeJean.deposit(500);

        /* depot de 1000EURO sur compte de Paul*/
        compteDePaul.deposit(1000);

        /* retrait de 10euro sur compte de jean*/
        compteDePaul.withdraw(10);

        /*virement de 75 du compte de jean sur celui de paul*/
        compteDeJean.transfertTo(75, compteDePaul);


        System.out.println("Hello world!");

        /* AFFICHAGE DES DEUX COMPTES*/

        System.out.println("Compte de Jean =" + compteDeJean.toString());

        System.out.println("Compte de Paul = " + compteDePaul.toString());


    }
}


