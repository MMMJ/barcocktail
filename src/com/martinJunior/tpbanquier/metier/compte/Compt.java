package com.martinJunior.tpbanquier.metier.compte;
import java.util.Scanner;
public class Compt {
        private int solde;
        private String titulaire;


        /*CONSTRUCTEUR*/
    public Compt (int solde, String nomTitulaire) {
        this.solde = solde;
        this.titulaire = nomTitulaire;
    }

    /* MISE EN PLACE DE MES GETTER ET SETTER*/
    public int getSolde(int solde){
        return this.solde;
    }

    public String getNom(){
        return this.titulaire;
    }

    public int setSolde(int solde){
        this.solde=solde;
        return solde;
    }
    public String setNom(){

        return this.titulaire;
    }

    /* MISE EN PLACE DE MES METHODES*/
        public void deposit(int montant){
            {solde += montant;}

        }
        public void withdraw(int montant){
            { solde -= montant;}
        }
        public void transfertTo(int montant, Compt destination){
            solde -= montant;
            destination.solde +=montant;
        }


    public String toString() {
        return "solde du compte :" + this.solde + "€";
    }
}



